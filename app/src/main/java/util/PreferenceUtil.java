package util;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import model.Celebrity;
import model.StoryDetail;

public class PreferenceUtil {


    private static final String FILE_NAME = "ROPOSO_PREFERENCES";
    private static final String PREF_KEY_CELEBRITY_LIST = "PREF_KEY_CELEBRITY_LIST";
    private static final String PREF_KEY_STORY_DETAILS = "PREF_KEY_STORY_DETAILS";
    private static SharedPreferences appSharedPrefs;


    public static SharedPreferences getSharedPreferences(Context context) {
        if (appSharedPrefs == null) {
            appSharedPrefs = context.getSharedPreferences(FILE_NAME,
                    Context.MODE_PRIVATE);
        }
        return appSharedPrefs;
    }


    public static boolean saveObjectToPrefs(String prefKey, Object object, Context context) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        try {
            Gson gson = new Gson();
            String json = gson.toJson(object);
            editor.putString(prefKey, json);
            editor.apply();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean saveCelebrity(List<Celebrity> celebrityList, Context context) {
        return saveObjectToPrefs(PREF_KEY_CELEBRITY_LIST, celebrityList, context);
    }

    public static boolean saveStoryDetails(List<StoryDetail> storyDetails, Context context) {
        return saveObjectToPrefs(PREF_KEY_STORY_DETAILS, storyDetails, context);
    }

    public static List<Celebrity> getCelebrityList(Context context) {
        Type type = new TypeToken<List<Celebrity>>() {}.getType();
        return getObjectFromPrefs(PREF_KEY_CELEBRITY_LIST, type , context);
    }

    public static List<StoryDetail> getStoryDetail(Context context){
        Type type = new TypeToken<List<StoryDetail>>() {}.getType();
        return getObjectFromPrefs(PREF_KEY_STORY_DETAILS, type, context);
    }

    public static <T> T getObjectFromPrefs(String prefKey, Type type, Context context) {
        String json = getSharedPreferences(context).getString(prefKey, null);
        if (json != null) {
            try {
                Gson gson = new Gson();
                T result = gson.fromJson(json, type);
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
