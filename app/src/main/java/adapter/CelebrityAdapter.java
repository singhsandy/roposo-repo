package adapter;


import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.sandeep.roposo.R;
import com.facebook.common.util.UriUtil;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.DraweeView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;

import java.util.List;
import model.Celebrity;

public class CelebrityAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<Celebrity> celebrityList;
    private  OnItemClickListener onItemClickListener;

    public CelebrityAdapter(Context context, List<Celebrity> celebrityList){
        this.celebrityList = celebrityList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.celebrity_list_item,parent,false);
        return new CelebrityHolder(view, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Celebrity celebrity = celebrityList.get(position);
        CelebrityHolder celebrityHolder = (CelebrityHolder) holder;
        celebrityHolder.celebrityTxt.setText(celebrity.getUserName());
        celebrityHolder.celebrityAboutTxt.setText(celebrity.getAbout());
        celebrityHolder.celebrityFollowTxt.setText(celebrity.getFollower()+"");
        celebrityHolder.celebrityFollowingTxt.setText(celebrity.getFollowing()+"");
        celebrityHolder.handleTxt.setText(celebrity.getHandle());
        celebrityHolder.followBtn.setChecked(celebrity.is_following());
        Uri imageUri = Uri.parse(celebrity.getImage());
        ImageRequest request = ImageRequest.fromUri(imageUri);
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setImageRequest(request)
                .setOldController(celebrityHolder.celebrityImage.getController()).build();
        celebrityHolder.celebrityImage.setController(controller);
    }

    @Override
    public int getItemCount() {
        return celebrityList.size();
    }

    public class CelebrityHolder extends RecyclerView.ViewHolder{

        TextView celebrityTxt;
        TextView celebrityAboutTxt, handleTxt;
        TextView celebrityFollowTxt, celebrityFollowingTxt;
        SimpleDraweeView celebrityImage;
        ToggleButton followBtn;

        public CelebrityHolder(View itemView, final OnItemClickListener onItemClickListener) {
            super(itemView);
            celebrityTxt = (TextView) itemView.findViewById(R.id.celebrityTxt);
            celebrityAboutTxt = (TextView) itemView.findViewById(R.id.celebrityAboutTxt);
            celebrityFollowTxt = (TextView) itemView.findViewById(R.id.follower);
            celebrityFollowingTxt = (TextView) itemView.findViewById(R.id.following);
            celebrityImage = (SimpleDraweeView) itemView.findViewById(R.id.celebrityImage);
            followBtn = (ToggleButton) itemView.findViewById(R.id.followBtn);
            handleTxt = (TextView) itemView.findViewById(R.id.handleTxt);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onItemClickListener!=null){
                        onItemClickListener.clickListener(getPosition());
                    }
                }
            });
            followBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(onItemClickListener!=null){
                        celebrityList.get(getAdapterPosition()).setIs_following(followBtn.isChecked());
                        onItemClickListener.followBtnClickListener(getAdapterPosition(),
                                                                   celebrityList.get(getAdapterPosition()));
                    }
                }
            });
        }
    }

    public void setCallback(OnItemClickListener clickListener){
        onItemClickListener = clickListener;
    }

    public interface OnItemClickListener{
        void clickListener(int position);
        void followBtnClickListener(int position,Celebrity celebrity);
    }
}
