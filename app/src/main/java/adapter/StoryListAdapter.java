package adapter;


import android.content.Context;
import android.graphics.PointF;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.sandeep.roposo.R;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;


import java.util.ArrayList;
import java.util.List;

import model.StoryDetail;

public class StoryListAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<StoryDetail> storyDetailList = new ArrayList<>();
    private OnItemClickListener onItemClickListener;

    public StoryListAdapter(Context context, List<StoryDetail> storyDetailList) {
        this.context = context;
        this.storyDetailList = storyDetailList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.story_detail_list_item,parent,false);
        return new StoryDetailsViewHolder(view, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        StoryDetailsViewHolder storyDetailsViewHolder = (StoryDetailsViewHolder) holder;
        StoryDetail storyDetail = storyDetailList.get(position);
        storyDetailsViewHolder.storyTitleTxt.setText(storyDetail.getTitle());
        storyDetailsViewHolder.descriptionTxt.setText(storyDetail.getDescription());
        storyDetailsViewHolder.likesTxt.setText(storyDetail.getLikes_count()+"");
        storyDetailsViewHolder.commentTxt.setText(storyDetail.getComment_count()+"");
        storyDetailsViewHolder.createdOnTxt.setText(storyDetail.getCreatedOn()+"");
        storyDetailsViewHolder.likeStoryBtn.setChecked(storyDetail.isLike_flag());
        ImageRequest request = ImageRequest.fromUri(storyDetail.getSi().toString());
        PointF focusPoint = new PointF(0.1f,0.1f);
        storyDetailsViewHolder.storyImageView
                .getHierarchy()
                .setActualImageFocusPoint(focusPoint);
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setImageRequest(request)
                .setOldController(storyDetailsViewHolder.storyImageView.getController()).build();
        storyDetailsViewHolder.storyImageView.setController(controller);

    }

    @Override
    public int getItemCount() {
        return storyDetailList.size();
    }

    public class StoryDetailsViewHolder extends RecyclerView.ViewHolder{

        TextView storyTitleTxt, descriptionTxt, createdOnTxt, likesTxt, commentTxt;
        SimpleDraweeView  storyImageView;
        ToggleButton likeStoryBtn;
        public StoryDetailsViewHolder(View itemView, final OnItemClickListener onItemClickListener) {
            super(itemView);
            storyTitleTxt = (TextView) itemView.findViewById(R.id.storyTitleTxt);
            descriptionTxt = (TextView) itemView.findViewById(R.id.descriptionTxt);
            storyImageView = (SimpleDraweeView) itemView.findViewById(R.id.storyImageView);
            createdOnTxt = (TextView) itemView.findViewById(R.id.createdOnTxt);
            likesTxt = (TextView) itemView.findViewById(R.id.likes);
            commentTxt = (TextView) itemView.findViewById(R.id.comments);
            likeStoryBtn = (ToggleButton) itemView.findViewById(R.id.followStoryBtn);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onItemClickListener!=null){
                        onItemClickListener.onClickListener(getAdapterPosition());
                    }
                }
            });
            likeStoryBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(onItemClickListener!=null){
                        storyDetailList.get(getAdapterPosition()).setLike_flag(likeStoryBtn.isChecked());
                        onItemClickListener.onFollowBtnClickListener(getAdapterPosition(),storyDetailList.get(getAdapterPosition()));
                    }
                }
            });
        }
    }

    public void setCallback(OnItemClickListener clickListener){
        this.onItemClickListener = clickListener;
    }

    public interface OnItemClickListener{
        void onClickListener(int position);
        void onFollowBtnClickListener(int position,StoryDetail storyDetail);
    }
}
