package activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.example.sandeep.roposo.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import adapter.StoryListAdapter;
import model.Celebrity;
import model.StoryDetail;
import util.PreferenceUtil;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CelebrityStoryListActivity extends AppCompatActivity
        implements StoryListAdapter.OnItemClickListener, StoryDetailsActivity.OnFollowBtnClickLister {

    @BindView(R.id.storyListRecyclerView)
    RecyclerView storyListRecyclerView;

    private Celebrity celebrity;
    private Gson gson;
    private StoryListAdapter storyListAdapter;
    private List<StoryDetail> storyDetailList = new ArrayList<>();
    private List<StoryDetail> selectedList = new ArrayList<>();
    private static OnClickListener onFollowClickLister;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.story_list_activity);
        ButterKnife.bind(this);
        gson = new Gson();
        StoryDetailsActivity.setCallback(this);
        getStoryDetailList();
        setToolbar();
        setRecyclerView();
    }

    private void setToolbar() {
        getSupportActionBar().setTitle(celebrity.getUserName());
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    private void setRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        storyListRecyclerView.setLayoutManager(layoutManager);
        storyListAdapter = new StoryListAdapter(this,selectedList);
        storyListAdapter.setCallback(this);
        storyListRecyclerView.setAdapter(storyListAdapter);
    }

    private void getStoryDetailList() {
        Intent intent  = getIntent();
        String celebrityJson = intent.getExtras().getString("CELEBRITY");
        celebrity = gson.fromJson(celebrityJson,Celebrity.class);
        storyDetailList = PreferenceUtil.getStoryDetail(this);
        for(StoryDetail detail: storyDetailList){
            if(detail.getDb().equals(celebrity.getId())){
                selectedList.add(detail);
            }
        }
    }

    @Override
    public void onClickListener(int position) {
        StoryDetail storyDetail = selectedList.get(position);
        String jsonStoryDetail = gson.toJson(storyDetail);
        Intent intent = new Intent(this, StoryDetailsActivity.class);
        intent.putExtra("Position",position);
        intent.putExtra("StoryDetail", jsonStoryDetail);
        intent.putExtra("CelebrityDetail", getIntent().getExtras().getString("CELEBRITY"));
        startActivity(intent);
    }



    public boolean onCreateOptionsMenu(Menu menu){
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_celebritydetail,menu);
        MenuItem checkable = menu.findItem(R.id.checkable_menu);
        checkable.setChecked(celebrity.is_following());
        if (celebrity.is_following()) {
            checkable.setTitle("Following");
        } else {
            checkable.setTitle("Follow");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.checkable_menu:
                if (celebrity.is_following()) {
                    item.setTitle("Follow");
                    celebrity.setIs_following(false);
               } else {
                    item.setTitle("Following");
                    celebrity.setIs_following(true);
                }
                item.setChecked(celebrity.is_following());
                onFollowClickLister.onFollowBtnClick(celebrity);
                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }


    @Override
    public void onFollowBtnClickListener(int position,StoryDetail storyDetail) {
        selectedList.set(position,storyDetail);
        storyDetailList.set(getIndex(storyDetail),storyDetail);
    }

    @Override
    public void onClickListener(int position, StoryDetail storyDetail) {
        selectedList.set(position,storyDetail);
        storyListAdapter.notifyItemChanged(position);
        int index = getIndex(storyDetail);
        storyDetailList.set(index,storyDetail);
    }

    private int getIndex(StoryDetail story) {
        for(StoryDetail storyDetail: storyDetailList){
            if(storyDetail.getId().equals(story.getId()))
                return storyDetailList.indexOf(storyDetail);
        }
        return 0;
    }


    public static void setCallback(OnClickListener clicklistener){
        onFollowClickLister = clicklistener;
    }

    public interface OnClickListener{
        void onFollowBtnClick(Celebrity celebrity);
    }

    @Override
    public void onStop(){
        super.onStop();
        PreferenceUtil.saveStoryDetails(storyDetailList,this);
    }

}
