package activity;

import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.sandeep.roposo.R;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import model.Celebrity;
import model.StoryDetail;


public class StoryDetailsActivity extends AppCompatActivity {

    @BindView(R.id.celebrityImage)
    SimpleDraweeView celebrityImageView;

    @BindView(R.id.celebrityTitleTxt)
    TextView celebrityTitleTxt;

    @BindView(R.id.storyTitleTxt)
    TextView storyTitleTxt;

    @BindView(R.id.storyVerbTxt)
    TextView storyVerbTxt;

    @BindView(R.id.storyDescription)
    TextView storyDescriptionTxt;


    @BindView(R.id.storyLikeTxt)
    TextView storyLikeTxt;

    @BindView(R.id.storyCommentTxt)
    TextView storyCommentTxt;

    @BindView(R.id.storyImageView)
    SimpleDraweeView storyImageView;


    @BindView(R.id.twitterHandle)
    TextView twitterHandleTxt;

    @BindView(R.id.followBtnStory)
    ToggleButton followStoryBtn;

    private StoryDetail storyDetail;
    private Celebrity celebrity;
    private static OnFollowBtnClickLister onFollowClickLister;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.story_detail_activity);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        Gson gson = new Gson();
        String jsonStoryDetail = intent.getExtras().getString("StoryDetail");
        String jsonCelebrityDetail = intent.getExtras().getString("CelebrityDetail");
        storyDetail = gson.fromJson(jsonStoryDetail,StoryDetail.class);
        celebrity = gson.fromJson(jsonCelebrityDetail, Celebrity.class);
        setView();
    }

    private void setView() {
        getSupportActionBar().setTitle(storyDetail.getTitle());
        getSupportActionBar().setHomeButtonEnabled(true);
        celebrityImageView.setImageURI(celebrity.getImage());
        celebrityTitleTxt.setText(celebrity.getUserName());
        twitterHandleTxt.setText(celebrity.getHandle());
        storyTitleTxt.setText(storyDetail.getTitle());
        storyDescriptionTxt.setText(storyDetail.getDescription());
        storyVerbTxt.setText(storyDetail.getVerb());
        storyLikeTxt.setText(storyDetail.getLikes_count()+"");
        storyCommentTxt.setText(storyDetail.getComment_count()+"");
        followStoryBtn.setChecked(storyDetail.isLike_flag());


        PointF focusPoint = new PointF(0.25f,0.40f);
        storyImageView
                .getHierarchy()
                .setActualImageFocusPoint(focusPoint);
        storyImageView.setImageURI(storyDetail.getSi().toString());

        followStoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storyDetail.setLike_flag(followStoryBtn.isChecked());
                onFollowClickLister.onClickListener(getIntent().getExtras().getInt("Position"),storyDetail);
            }
        });

    }

    public static void setCallback(OnFollowBtnClickLister clicklistener){
        onFollowClickLister = clicklistener;
    }

    public interface OnFollowBtnClickLister{
        void onClickListener(int position,StoryDetail storyDetail);
    }

}
