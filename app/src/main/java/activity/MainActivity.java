package activity;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.sandeep.roposo.R;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import adapter.CelebrityAdapter;
import helper.ReadDataHelper;
import model.Celebrity;
import util.PreferenceUtil;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements CelebrityAdapter.OnItemClickListener, CelebrityStoryListActivity.OnClickListener {

    private ReadDataHelper mReadDatahelper;
    private static final String TAG = MainActivity.class.getName();
    private List<Celebrity> celebrityList = new ArrayList<>();

    @Nullable
    @BindView(R.id.celebrityRecyclerView)
    RecyclerView celebrityRecyclerView;
    CelebrityAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Fresco.initialize(this);
        CelebrityStoryListActivity.setCallback(this);
        mReadDatahelper = new ReadDataHelper(this);
        if(PreferenceUtil.getCelebrityList(this) == null || PreferenceUtil.getStoryDetail(this) ==null)
            mReadDatahelper.loadJSONFromAsset();
        celebrityList = PreferenceUtil.getCelebrityList(this);
        setRecyclerView();
        Log.d(TAG, "onCreate: " + celebrityList.size());
    }

    private void setRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        celebrityRecyclerView.setLayoutManager(layoutManager);
        adapter = new CelebrityAdapter(this,celebrityList);
        adapter.setCallback(this);
        celebrityRecyclerView.setAdapter(adapter);
    }

    @Override
    public void clickListener(int position) {
        Gson gson = new Gson();
        String celebrityJson = gson.toJson(celebrityList.get(position));
        Intent intent = new Intent(this, CelebrityStoryListActivity.class );
        intent.putExtra("CELEBRITY", celebrityJson);
        startActivity(intent);
    }

    @Override
    public void followBtnClickListener(int position, Celebrity celebrity) {
        celebrityList.set(position, celebrity);
    }

    @Override
    public void onFollowBtnClick(Celebrity celebrity) {
        int index = getIndex(celebrity);
        celebrityList.set(index,celebrity);
        adapter.notifyItemChanged(index);
    }

    private int getIndex(Celebrity celeb) {
        for(Celebrity celebrity: celebrityList){
            if(celebrity.getId().equals(celeb.getId()))
                return celebrityList.indexOf(celebrity);
        }
        return 0;
    }

    @Override
    public void onStop(){
        super.onStop();
        PreferenceUtil.saveCelebrity(celebrityList,this);
    }
}
