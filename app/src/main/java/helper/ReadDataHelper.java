package helper;


import android.content.Context;
import android.content.ContextWrapper;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import model.Celebrity;
import model.StoryDetail;
import util.PreferenceUtil;

public class ReadDataHelper extends ContextWrapper {

    private Context context;
    private List<Celebrity> celebrityList = new ArrayList<>();
    private List<StoryDetail> storyDetailList = new ArrayList<>();

    public ReadDataHelper(Context context) {
        super(context);
        this.context = context;
    }

    public void loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = context.getAssets().open("iOS_Android Data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        saveDataintoSharedPreferences(json);
    }

    private void saveDataintoSharedPreferences(String json){
        Gson gson = new Gson();
        try {
            JSONArray jsonArray = new JSONArray(json);
            int length = jsonArray.length();
            for (int i = 0; i < length; i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                if (jsonObject.has("username")) {
                    Celebrity celebrity = gson.fromJson(jsonObject.toString(),Celebrity.class);
                    celebrityList.add(celebrity);
                } else {
                    StoryDetail detail = gson.fromJson(jsonObject.toString(),StoryDetail.class);
                    storyDetailList.add(detail);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        PreferenceUtil.saveCelebrity(celebrityList,context);
        PreferenceUtil.saveStoryDetails(storyDetailList,context);
    }
}
